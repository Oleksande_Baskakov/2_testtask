(function(){

const body = document.querySelector('body')
let pointedElement
let warning = false;
const shadowDiv = document.createElement('div')
shadowDiv.attachShadow({mode: 'open'})
const wrapp = document.createElement('div');

wrapp.innerHTML=`
<div style='position: fixed; right: 0;box-shadow: 3px 4px black; top:0; width: 280px; height: 120px; background-color: white;border-radius: 15px;padding: 10px;z-index: 9999;border: 1px solid black;' class='selector_seeker__shadow'>
	<p style="margin: 0;cursor: default;margin-bottom: 8px;" class='selector_seeker__text'>Searching element by selectors</p>
	<div style="position: absolute; top: 5px; right: 5px;" class='selector_seeker__image'>
    <img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAXVBMVEX///8AAACcnJzZ2dmampr5+fna2tqfn59PT09UVFT8/PwEBARRUVFMTEz09PTo6OgeHh4lJSV4eHgUFBQ5OTnh4eFjY2OEhITu7u5dXV0PDw/Jyclvb2+0tLRDQ0P5o4h/AAAD60lEQVR4nO2djVajMBCFBRft4n/Vult13/8xXUppktNQIGT+cu73AuM9vZcMnYy9ugIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIBi2ifR8o/k5evn6uWbusg429fq7Zq0Ql11/CKtMU7zcCj/Tliifa4EJTa/++pbwhpP1ZGasMgY7f2x+I6yyougxM1Q+4OyyvdQhd2ozUng/g9poVpI4pDB/9wQl3ISWY36cCpLe1h03Ap8iu2GUaCERM+iHAL5s9hyC2TPIq9FeziN2riHDPVT1IfPqOwZHGAz6r2QQDaJEhkc4Mhic/oE7/gFcmSRsVWLQ25UzlYtDq1RmVu1OJQSxY6JELosCrRqcciyqMGiPTRGda3ancxT1IfCqEoyOEBgVD0W7cktUcUxEZI3i41csz1Oziy20q1anIxGlW/V4uQyaqtVYC6Jyo6JkBxGbTU+ZBwZJOo7JkLWGtUNXxS0anHWHRrib/RzWGVU7RbtSZeosFWLk5pFla1anLQsKm3V4iQZ1YpFe5YbVWj4ks5So6pu1eIsNKqdh4xjkURbGRyYn0Xh4Us6c7NoolWLM9Ooel94p5ljVDOtWpxpiQaPiZCpLKoZvqQzkUXbFu25ZFRVw5d0xo1qPoMDo0a12KrFiUs0fkyExLJotlWLc55Fw61anDOjWm7V4oRGLSqDA77EYo6JEM+of4sU6EssVKBv1OMxUcZT1Kcu+xPsqEsX6Bt1X55FOxr3FK1upf8YCrw3+kpu0ZaSTRUgsYVKijd8KfNTdK3avkyJ3j2ZG/dELcmowduE0BYqJd49mcNBL7GFSsr5G73UujQVkTf6oowaf6MvyKhjb/TFSBwfvpSSxQtfOhWRxcvDlwKMOvWtmn2jTg5frBt1xhe/piXOG74YzuLc4YvdLM4evhg16pLhi0mjLhu+GJS4dPPFnlEXzweNSUzZfDFl1LQBqKVDI3GEbcao6TN6IxLXbL6YyOK6zRcLWVx5T0a9UdvVmy/KjZrjnoxqo+ZZUtZs1ExXudRKzLekrDSLOa9T6sxi1uuUCo2a+zqlOqPmv06pTCLF5ouuLJLc+FWURaolZTVGpbvSrMWohJsvOoxKeutegUTqzRfxLNJvvkhnkWHzRdSoPJsvgkbl2nwRk8i3pCyVRcblLJEs8i4pCxiVe/uM36js+4PMRpVYkGSVKPNvxxizKPVvx/iyKLak7CTS7kxtZT7BDmfUT8Iqj69iAr1P8YuwyOnXAUUWJAeJpL8O+CYo8CSR9MS47irIbYAesvivJa3xvt190P7+4EU+v3Y1rUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACs4gdR1xkzUSKuXQAAAABJRU5ErkJggg==' style="width: 20px; height: 20px;border-radius: 40px;" class='selector_seeker__crossbar'>
	</div>
	<input type='text' class='selector_seeker__input' style='margin-bottom:15px;border-radius: 8px;margin-right: 15px;'><button style="cursor: pointer;border-radius: 40px;" class='selector_seeker__search_button'>Search</button>
	<div style='padding-top: 20px; position: relative' class='selector_seeker__buttons'>
    <button style="cursor: pointer;border-radius: 40px;" class='selector_seeker__prev_elem_button'>Prev. elem.</button>
    <button style="cursor: pointer;border-radius: 40px;" class='selector_seeker__next_elem_button'>Next elem.</button>
    <button style="cursor: pointer;border-radius: 40px;" class='selector_seeker__parent_button'>Parent</button>
    <button style="cursor: pointer;border-radius: 40px;" class='selector_seeker__child_button'>Child</button>
	</div>
</div>
`
shadowDiv.appendChild(wrapp);
shadowDiv.shadowRoot.appendChild(wrapp);
body.prepend(shadowDiv);

let searchButton = wrapp.querySelector('.selector_seeker__search_button');
let nextElemButton = wrapp.querySelector('.selector_seeker__next_elem_button');
let prevElemButton = wrapp.querySelector('.selector_seeker__prev_elem_button');
let parentButton = wrapp.querySelector('.selector_seeker__parent_button');
let childButton = wrapp.querySelector('.selector_seeker__child_button');
let crossbarButton = wrapp.querySelector('.selector_seeker__crossbar');
let blockWithButtons = wrapp.querySelector('.selector_seeker__buttons');


function changeButtonsStatus () {
		prevElemButton.disabled = pointedElement.previousElementSibling ? false : true;
		nextElemButton.disabled = pointedElement.nextElementSibling ? false : true;
		parentButton.disabled = pointedElement.parentElement ? false : true;
		childButton.disabled = pointedElement.firstElementChild ? false : true;
};

function onCloseHandler () {
    if (pointedElement) {
			pointedElement.style = 'outline: none';
		} 
    wrapp.querySelector('.selector_seeker__shadow').remove()
}

function actionSeparatorPointerPerekluchatorButtonStatusMenyator (action) {
    let newSearchSubject
    switch(action) {
        case 'Prev. elem.': newSearchSubject = pointedElement.previousElementSibling;
        break
        case 'Next elem.': newSearchSubject = pointedElement.nextElementSibling;
        break
        case 'Parent': newSearchSubject = pointedElement.parentElement;
        break
        case 'Child': newSearchSubject = pointedElement.firstElementChild;
        break
        default: newSearchSubject = null;
    }
    if(newSearchSubject) {
        pointedElement.style = 'outline: none';
        newSearchSubject.style = 'outline: 1px solid red';
        newSearchSubject.scrollIntoView({block: 'center'});
        pointedElement = newSearchSubject
    }
    changeButtonsStatus()
}

function seekAndPoint(input) {
    let searchSubject = document.querySelector(`${input}`);
    if(searchSubject) {
        searchSubject.style = 'outline: 1px solid red';
        searchSubject.scrollIntoView({block: 'center'});
        pointedElement = searchSubject;
    } else {
        if (!warning) {
            wrapp.querySelector('.selector_seeker__prev_elem_button').insertAdjacentHTML('beforebegin','<p style="color:blue;font-size: 13px;margin: 0; position: absolute; left:0;top:0" class= "selector_seeker__warning2">Can not find element with this selector</p>')
            warning = true;
            let warningMessage = wrapp.querySelector('.selector_seeker__warning2')
            setTimeout(() => {
                warningMessage.remove();
                warning = false;
            }, 2000 )
        }
    }
}




function searchButtonClickedHandler () {
    if(pointedElement) pointedElement.style = 'outline: none';
    let inputText = wrapp.getElementsByClassName('selector_seeker__input')[0].value
    if (inputText){
        seekAndPoint(inputText)
    } else {
        if (!warning) {
            wrapp.querySelector('.selector_seeker__prev_elem_button').insertAdjacentHTML('beforebegin','<p style="color:blue;margin: 0; position: absolute;font-size: 13px; left:0;top:0" class= "selector_seeker__warning">Please, write something</p>')
            warning = true;
            let warningMessage = wrapp.querySelector('.selector_seeker__warning')
            setTimeout(() => {
                warningMessage.remove();
                warning = false;
            }, 1000 )
        }

    }
    changeButtonsStatus()

}


blockWithButtons.addEventListener('click', e => actionSeparator(e.target.innerText))
searchButton.addEventListener('click', searchButtonClickedHandler)
crossbarButton.addEventListener('click', onCloseHandler)


dragElement(wrapp.querySelector('.selector_seeker__shadow'));

function dragElement(elem) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    elem.onmousedown = dragMouseDown;

  function dragMouseDown(e) {
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    elem.style.top = (elem.offsetTop - pos2) + "px";

    if((elem.offsetTop - pos2) < 0) {
        elem.style.top = 0 + 'px'
    }

    elem.style.left = (elem.offsetLeft - pos1) + "px";
    if((elem.offsetLeft - pos1) < 0) elem.style.left = 0 +'px'
    if(elem.style.left.slice(0, -2) > (document.documentElement.clientWidth - 270)) elem.style.left = (document.documentElement.clientWidth - 270) +'px'
    if(elem.style.top.slice(0, -2) > (document.documentElement.clientHeight - 120)) elem.style.top = (document.documentElement.clientHeight - 120) +'px'
  }

  function closeDragElement() {
    document.onmouseup = null;
    document.onmousemove = null;
  }
}
})()

/*
(function(){function a(){f.previousElementSibling?m.removeAttribute("disabled"):m.setAttribute("disabled","disabled"),f.nextElementSibling?l.removeAttribute("disabled"):l.setAttribute("disabled","disabled"),f.parentElement?n.removeAttribute("disabled"):n.setAttribute("disabled","disabled"),f.firstElementChild?o.removeAttribute("disabled"):o.setAttribute("disabled","disabled")}function b(){f&&(f.style="outline: none"),j.querySelector(".selector_seeker__shadow").remove()}function c(b){let c="Prev. elem."===b?f.previousElementSibling:"Next elem."===b?f.nextElementSibling:"Parent"===b?f.parentElement:"Child"===b?f.firstElementChild:null;c&&(f.style="outline: none",c.style="outline: 1px solid red",c.scrollIntoView({block:"center"}),f=c),a()}function d(a){let b=document.querySelector(`${a}`);if(console.log(b),b)b.style="outline: 1px solid red",b.scrollIntoView({block:"center"}),f=b;else if(!h){j.querySelector(".selector_seeker__prev_elem_button").insertAdjacentHTML("beforebegin","<p style=\"color:blue;font-size: 13px;margin: 0; position: absolute; left:0;top:0\" class= \"selector_seeker__warning2\">Can not find element with this selector</p>"),h=!0;let a=j.querySelector(".selector_seeker__warning2");setTimeout(()=>{a.remove(),h=!1},2e3)}}function e(){f&&(f.style="outline: none");let b=j.getElementsByClassName("selector_seeker__input")[0].value;if(b)d(b);else if(!h){j.querySelector(".selector_seeker__prev_elem_button").insertAdjacentHTML("beforebegin","<p style=\"color:blue;margin: 0; position: absolute;font-size: 13px; left:0;top:0\" class= \"selector_seeker__warning\">Please, write something</p>"),h=!0;let a=j.querySelector(".selector_seeker__warning");setTimeout(()=>{a.remove(),h=!1},1e3)}a()}let f,g=document.querySelector("body"),h=!1,i=document.createElement("div");i.attachShadow({mode:"open"});const j=document.createElement("div");j.innerHTML=`
<div style='position: fixed; right: 0;box-shadow: 3px 4px black; top:0; width: 280px; height: 120px; background-color: white;border-radius: 15px;padding: 10px;z-index: 9999;border: 1px solid black;' class='selector_seeker__shadow'>
<p style="margin: 0;cursor: default;margin-bottom: 8px;" class='selector_seeker__text'>Searching element by selectors</p>
<div style="position: absolute; top: 5px; right: 5px;" class='selector_seeker__image'>
    <img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAXVBMVEX///8AAACcnJzZ2dmampr5+fna2tqfn59PT09UVFT8/PwEBARRUVFMTEz09PTo6OgeHh4lJSV4eHgUFBQ5OTnh4eFjY2OEhITu7u5dXV0PDw/Jyclvb2+0tLRDQ0P5o4h/AAAD60lEQVR4nO2djVajMBCFBRft4n/Vult13/8xXUppktNQIGT+cu73AuM9vZcMnYy9ugIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIBi2ifR8o/k5evn6uWbusg429fq7Zq0Ql11/CKtMU7zcCj/Tliifa4EJTa/++pbwhpP1ZGasMgY7f2x+I6yyougxM1Q+4OyyvdQhd2ozUng/g9poVpI4pDB/9wQl3ISWY36cCpLe1h03Ap8iu2GUaCERM+iHAL5s9hyC2TPIq9FeziN2riHDPVT1IfPqOwZHGAz6r2QQDaJEhkc4Mhic/oE7/gFcmSRsVWLQ25UzlYtDq1RmVu1OJQSxY6JELosCrRqcciyqMGiPTRGda3ancxT1IfCqEoyOEBgVD0W7cktUcUxEZI3i41csz1Oziy20q1anIxGlW/V4uQyaqtVYC6Jyo6JkBxGbTU+ZBwZJOo7JkLWGtUNXxS0anHWHRrib/RzWGVU7RbtSZeosFWLk5pFla1anLQsKm3V4iQZ1YpFe5YbVWj4ks5So6pu1eIsNKqdh4xjkURbGRyYn0Xh4Us6c7NoolWLM9Ooel94p5ljVDOtWpxpiQaPiZCpLKoZvqQzkUXbFu25ZFRVw5d0xo1qPoMDo0a12KrFiUs0fkyExLJotlWLc55Fw61anDOjWm7V4oRGLSqDA77EYo6JEM+of4sU6EssVKBv1OMxUcZT1Kcu+xPsqEsX6Bt1X55FOxr3FK1upf8YCrw3+kpu0ZaSTRUgsYVKijd8KfNTdK3avkyJ3j2ZG/dELcmowduE0BYqJd49mcNBL7GFSsr5G73UujQVkTf6oowaf6MvyKhjb/TFSBwfvpSSxQtfOhWRxcvDlwKMOvWtmn2jTg5frBt1xhe/piXOG74YzuLc4YvdLM4evhg16pLhi0mjLhu+GJS4dPPFnlEXzweNSUzZfDFl1LQBqKVDI3GEbcao6TN6IxLXbL6YyOK6zRcLWVx5T0a9UdvVmy/KjZrjnoxqo+ZZUtZs1ExXudRKzLekrDSLOa9T6sxi1uuUCo2a+zqlOqPmv06pTCLF5ouuLJLc+FWURaolZTVGpbvSrMWohJsvOoxKeutegUTqzRfxLNJvvkhnkWHzRdSoPJsvgkbl2nwRk8i3pCyVRcblLJEs8i4pCxiVe/uM36js+4PMRpVYkGSVKPNvxxizKPVvx/iyKLak7CTS7kxtZT7BDmfUT8Iqj69iAr1P8YuwyOnXAUUWJAeJpL8O+CYo8CSR9MS47irIbYAesvivJa3xvt190P7+4EU+v3Y1rUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACs4gdR1xkzUSKuXQAAAABJRU5ErkJggg==' style="width: 20px; height: 20px;border-radius: 40px;" class='selector_seeker__crossbar'>
</div>
<input type='text' class='selector_seeker__input' style='margin-bottom:15px;border-radius: 8px;margin-right: 15px;'><button style="cursor: pointer;border-radius: 40px;" class='selector_seeker__search_button'>Search</button>
<div style='padding-top: 20px; position: relative' class='selector_seeker__buttons'>
    <button style="cursor: pointer;border-radius: 40px;" class='selector_seeker__prev_elem_button'>Prev. elem.</button>
    <button style="cursor: pointer;border-radius: 40px;" class='selector_seeker__next_elem_button'>Next elem.</button>
    <button style="cursor: pointer;border-radius: 40px;" class='selector_seeker__parent_button'>Parent</button>
    <button style="cursor: pointer;border-radius: 40px;" class='selector_seeker__child_button'>Child</button>
</div>


</div>
`,i.appendChild(j),i.shadowRoot.appendChild(j),g.prepend(i),console.log(j.querySelector(".selector_seeker__crossbar"));let k=j.querySelector(".selector_seeker__search_button"),l=j.querySelector(".selector_seeker__next_elem_button"),m=j.querySelector(".selector_seeker__prev_elem_button"),n=j.querySelector(".selector_seeker__parent_button"),o=j.querySelector(".selector_seeker__child_button"),p=j.querySelector(".selector_seeker__crossbar"),q=j.querySelector(".selector_seeker__buttons");q.addEventListener("click",a=>c(a.target.innerText)),k.addEventListener("click",e),p.addEventListener("click",b),function(a){function b(b){d=g-b.clientX,f=h-b.clientY,g=b.clientX,h=b.clientY,a.style.top=a.offsetTop-f+"px",0>a.offsetTop-f&&(a.style.top="0px"),a.style.left=a.offsetLeft-d+"px",0>a.offsetLeft-d&&(a.style.left="0px"),a.style.left.slice(0,-2)>document.documentElement.clientWidth-270&&(a.style.left=document.documentElement.clientWidth-270+"px"),a.style.top.slice(0,-2)>document.documentElement.clientHeight-120&&(a.style.top=document.documentElement.clientHeight-120+"px")}function c(){document.onmouseup=null,document.onmousemove=null}var d=0,f=0,g=0,h=0;a.onmousedown=function(a){g=a.clientX,h=a.clientY,document.onmouseup=c,document.onmousemove=b}}(j.querySelector(".selector_seeker__shadow"))})();
*/